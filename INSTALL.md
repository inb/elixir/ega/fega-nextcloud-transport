# Installation instructions

This software requires having Python 3. Once you have cloned this repo, you should run next instructions

```
python3 -m venv .f
source .f/bin/activate
pip install --upgrade pip wheel
pip install -r requirements.txt -c constraints.txt
```