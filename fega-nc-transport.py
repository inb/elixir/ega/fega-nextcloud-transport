#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import argparse
import base64
import configparser
import csv
import datetime
import inspect
import io
import logging
import os
import sys
from typing import List, Optional, Sequence, Union
import urllib.parse
import uuid

import requests
import pprint
import textwrap
import re

import crypt4gh.header  # type: ignore[import]
import crypt4gh.lib # type: ignore[import]
import crypt4gh.keys.kdf    # type: ignore[import]
import crypt4gh.keys.c4gh   # type: ignore[import]
from crypt4gh.keys import ssh   # type: ignore[import]

from extended_nc_client.extended_nc_client import ExtendedNextcloudClient

class Crypt4GHReReader(io.RawIOBase):
    '''
    This implementation has been inspired both on
    
    https://github.com/EGA-archive/crypt4gh/blob/2ba98a7cea96e8fb337b17310cc1a226ad3b3e65/crypt4gh/lib.py#L405-L422
    
    and
    
    https://github.com/python/cpython/blob/a43fd45918cef48187053d7c56d3b9bb902151d0/Lib/socket.py#L626-L740
    '''
    def __init__(self, infile, private_key: bytes, publish_keys: Union[bytes, List[bytes]], trim: bool = False, header: Optional[bytearray] = None):
        '''Extract header packets from infile and generate another one to outfile.
        The encrypted data section is only copied from infile to outfile.'''
        super().__init__()
        # Getting a logger focused on specific classes
        self.logger = logging.getLogger(dict(inspect.getmembers(self))['__module__'] + '::' + self.__class__.__name__)
        
        # Decrypt and re-encrypt the header
        self._infile = infile
        instream = open(infile, mode="rb")
        # If header is given separated, use it instead of fetching it
        # from the file
        if header:
            hstream = io.BytesIO(header)
        else:
            hstream = instream
        header_packets = crypt4gh.header.parse(hstream)
        
        if not isinstance(publish_keys, (list, tuple)):
            publish_keys = [ publish_keys ]
        
        orig_keys = [ (0, private_key, None) ]
        recipient_keys = [
            (0, private_key, publish_key)
            for publish_key in publish_keys
        ]
        
        packets = crypt4gh.header.reencrypt(header_packets, orig_keys, recipient_keys, trim=trim)
        
        # Right now, hard-coded
        self._mode = "rb"
        self._reading = True
        self._writing = False
        self._serialized_header: bytes = crypt4gh.header.serialize(packets)
        self._instream = instream
    
    def readinto(self, b):
        self._checkClosed()
        self._checkReadable()
        
        if self._serialized_header is not None:
            len_b = len(b)
            len_h = len(self._serialized_header)
            if len_b > len_h:
                # Reading the header and a part outside
                b[0:len_h] = self._serialized_header
                self._serialized_header = None
                
                frag_b = self._instream.read(len_b - len_h)
                len_frag_b = len(frag_b)
                b[len_h:len_h+len_frag_b] = frag_b
                
                return len_h + len_frag_b
            else:
                # Still reading the header
                b[0:len_b] = self._serialized_header[0:len_b]
                if len_b == len_h:
                    self._serialized_header = None
                else:
                    self._serialized_header = self._serialized_header[len_b:]
                
                return len_b
                # 
        else:
            # Fast path
            return self._instream.readinto(b)
    
    def write(self, b):
        self._checkClosed()
        # It should fail
        self._checkWritable()
        
        # it is a no-op
        pass
        
    def readable(self):
        if self.closed:
            raise ValueError("I/O operations on closed stream")
        
        return self._reading
    
    def writable(self):
        if self.closed:
            raise ValueError("I/O operations on closed stream")
        
        return self._writing
    
    def seekable(self):
        if self.closed:
            raise ValueError("I/O operations on closed stream")
        
        return super().seekable()
    
    def fileno(self):
        self._checkClosed()
        
        return self._instream.fileno()
    
    @property
    def mode(self):
        return self._mode
    
    @property
    def name(self):
        if not self.closed:
            return self._infile
        
        return ''
    
    def close(self):
        if self.closed:
            return
        
        super().close()
        self._serialized_header = None
        self._instream.close()
        self._instream = None

def parse_public_crypt4gh_key(lines_raw: Union[bytes, str, Sequence[Union[bytes, str]]], logger: logging.Logger = logging) -> bytes:
    '''Parses a public key from its string representation.'''
    
    if isinstance(lines_raw, str):
        lines_raw = lines_raw.encode('utf-8')
    
    if isinstance(lines_raw, bytes):
        lines_split = lines_raw.splitlines()
    else:
        lines_split = lines_raw
    if not isinstance(lines_split, (list, tuple)):
        raise NotImplementedError(f'Do not know how to deal with {lines_raw}')
        
    lines = []
    for l in lines_split:
        line = l.strip()
        if line:
            if not isinstance(line, bytes):
                line = line.encode('utf-8')
            lines.append(line)
    
    if len(lines) == 0:
        raise ValueError('Empty key')
    
    first_line = lines[0]

    if b'CRYPT4GH' in first_line: # it's Crypt4GH key
        logger.info('Loading a Crypt4GH public key')
        return base64.b64decode(b''.join(lines[1:-1]))

    if first_line[:4] == b'ssh-': # It's an SSH key
        logger.info('Loading an OpenSSH public key')
        return ssh.get_public_key(first_line)

    raise NotImplementedError('Unsupported key format')
        
class ContentExporter:
    def __init__(self, nextcloud_url, nextcloud_user, nextcloud_token, nextcloud_base_directory, publish_key_or_filename_key, vault_key_filename, vault_passphrase = None, retention_tag_name = 'remove_in_14'):
        # Getting a logger focused on specific classes
        self.logger = logging.getLogger(dict(inspect.getmembers(self))['__module__'] + '::' + self.__class__.__name__)
        
        # This set is used to record the directories which have already
        # been created (or ensured they exist)
        self._ensured_paths = set()
        
        self.nextcloud_url = nextcloud_url
        self.nextcloud_user = nextcloud_user
        self.nextcloud_token = nextcloud_token
        self.base_directory = nextcloud_base_directory
        
        # Getting the keys in memory
        # eading the public key to be reused on recoding
        publish_key = None
        try:
            if type(publish_key_or_filename_key)==str and os.path.exists(publish_key_or_filename_key):
                publish_key = crypt4gh.keys.get_public_key(publish_key_or_filename_key)
            else:
                publish_key = parse_public_crypt4gh_key(publish_key_or_filename_key, self.logger)
        except Exception as e:
            message = f"Users public key {publish_key_or_filename_key} is neither a readable file or a valid public key"
            self.logger.exception(message)
            raise ValueError(f"Users public key {publish_key_or_filename_key} is neither a readable file or a valid public key") from e
        
        self.publish_keys = [ publish_key ]
        
        self.vault_key_filename = vault_key_filename
        self.vault_dec_key = crypt4gh.keys.get_private_key(vault_key_filename, lambda: vault_passphrase)
        
        self.chunk_size = 65536
        
        self.enc = ExtendedNextcloudClient(nextcloud_url, dav_endpoint_version=2)
        self.enc.login(nextcloud_user, nextcloud_token)
        
        # Creating the retention system tag to be used to label
        if retention_tag_name is not None:
            retention_tag_id = self.enc.get_systemtag(retention_tag_name)
            if retention_tag_id is None:
                retention_tag_id = self.enc.create_systemtag(retention_tag_name)
        else:
            retention_tag_id = None
        
        self.ret_tag_name = retention_tag_name
        self.ret_tag_id = retention_tag_id
    
    def create_remote_path(self, reldir: Optional[str] = None, name: Optional[str] = None):
        # If the name is not defined, generate a random, new one
        if name is None:
            name = str(uuid.uuid4())
        
        if reldir:
            relretval = reldir + '/' + name
        else:
            relretval = name
        
        retval = self.base_directory + '/' + relretval
        retvalobj = None
        
        if retval not in self._ensured_paths:
            encoded_path = urllib.parse.quote(retval)
            
            path_info = self.enc.ensure_tree_exists(encoded_path)
            
            if path_info and (self.ret_tag_id is not None):
                self.enc.add_tag(path_info, tag_name=self.ret_tag_name)
            
            # import pprint
            # pprint.pprint(created)
            # pprint.pprint(retvalobj)
            
            # Adding all the prefixes of the path
            self._ensured_paths.add(retval)
            rslash = retval.rfind('/')
            while rslash > 0:
                self._ensured_paths.add(retval[0:rslash])
                rslash = retval.rfind('/', 0, rslash)

        return retvalobj, retval, relretval
    
    def _chunked_uploader(self, local_file, uplodir: Optional[str] = None, destname: Optional[str] = None, header: Optional[bytearray] = None) -> bool:
        if destname is None:
            destname = os.path.basename(local_file)
        
        
        destpath = urllib.parse.quote(self.base_directory) + '/'
        if uplodir:
            destpath += urllib.parse.quote(uplodir) + '/'
            
        destpath += urllib.parse.quote(destname, safe='')
        
        self.logger.debug(f'{local_file} -> {destpath}')
        timestamp = int(os.path.getmtime(local_file))
        retval = None
        try:
            self.logger.debug(f"Trying {local_file} as encrypted")
            with Crypt4GHReReader(local_file, self.vault_dec_key, self.publish_keys, header=header) as cR:
                with io.BufferedReader(cR, self.chunk_size) as crR:
                    retval = self.enc.put_stream(crR, destpath, remote_timestamp=timestamp)
        except ValueError as e:
            # It was not encrypted, falling back
            #import traceback
            #traceback.print_exc()
            self.logger.debug(f"Guessed {local_file} as encrypted")
            with open(local_file, mode='rb') as uH:
                retval = self.enc.put_stream(uH, destpath, remote_timestamp=timestamp)
        
        if retval and (self.ret_tag_id is not None):
            path_info = self.enc.file_info(destpath)
            if path_info:
                self.enc.add_tag(path_info, tag_name=self.ret_tag_name)
        
        return retval
    
    def _chunked_file_batch_uploader(self, files_to_process) -> Sequence[bool]:
        retvals = []
        self.logger.debug(f'{len(files_to_process)} files to upload')
        for local_file , upload_dir, destname, header in files_to_process:
            self.logger.debug(f'{local_file} {upload_dir} {destname} {header}')
            retvals.append(self._chunked_uploader(local_file, upload_dir, destname, header))
        
        return retvals
    
    def content_uploader_from_dir(self, local_path, uplodir: Optional[str] = None, destname: Optional[str] = None):
        if os.path.islink(local_path):
            # Skipping symlinks
            return
        
        files_to_process = []
        dirs_to_process = []
        if os.path.isfile(local_path):
            # The default should be the filename
            if destname is None:
                destname = os.path.basename(local_path)
            
            _ , remote_path, rel_remote_path = self.create_remote_path(name=uplodir)
            files_to_process.append((local_path, uplodir, destname, None))
        elif os.path.isdir(local_path):
            # The default should be a random filename
            dirs_to_process.append((local_path, uplodir, destname))
        
        retval_reldir = None
        retval_relreldir = None
        while len(dirs_to_process) > 0:
            new_dirs_to_process = []
            for local_dir, upload_base_dir, upload_dir_basename in dirs_to_process:
                self.logger.debug(f'{local_dir} {upload_base_dir} {upload_dir_basename}')
                _ , remote_path, rel_remote_path = self.create_remote_path(upload_base_dir, upload_dir_basename)
                if retval_reldir is None:
                    retval_reldir = remote_path
                    retval_relreldir = rel_remote_path
                for entry in os.scandir(local_dir):
                    # Skipping problematic files,
                    if not entry.name.startswith('.'):
                        if entry.is_file():
                            files_to_process.append((entry.path, rel_remote_path, entry.name, None))
                        elif entry.is_dir():
                            new_dirs_to_process.append((entry.path, rel_remote_path, entry.name))
            
            # Now, rotation time
            dirs_to_process = new_dirs_to_process
        
        retvals = self._chunked_file_batch_uploader(files_to_process)
        
        return retvals, retval_reldir, retval_relreldir
    
    def content_uploader_from_manifest(self, local_path, uplodir: Optional[str] = None, destname: Optional[str] = None, local_col: str = 'local', remote_col: str = 'remote', header_col: str = 'header'):
        files_to_process = []
        dirs_to_process = []
        
        retval_reldir = None
        retval_relreldir = None
        # Reading the CSV file
        local_path_dirname = os.path.dirname(os.path.abspath(local_path))
        with open(local_path, mode="r", newline='', encoding="utf-8") as fH:
            dialect = csv.Sniffer().sniff(fH.read(1024))
            fH.seek(0)
            reader = csv.DictReader(fH, dialect=dialect)
            for row in reader:
                rel_local_filename = row[local_col]
                if os.path.isabs(rel_local_filename):
                    local_filename = rel_local_filename
                else:
                    local_filename = os.path.normpath(os.path.join(local_path_dirname, rel_local_filename))
                
                if retval_relreldir is None:
                    # Declaring the remote path
                    _ , retval_reldir, retval_relreldir = self.create_remote_path()
                
                # Remote path must always be relative
                remote_filename = row[remote_col].lstrip('/')
                remote_dirname = os.path.dirname(remote_filename)
                if remote_dirname == '':
                    remote_dirname = retval_relreldir
                else:
                    remote_dirname = os.path.join(retval_relreldir, remote_dirname)
                remote_basename = os.path.basename(remote_filename)
                
                if os.path.exists(local_filename):
                    # Optional header, as it could not make sense
                    hexheader = row.get(header_col)
                    if hexheader:
                        header = bytearray.fromhex(hexheader)
                    else:
                        header = None
                    if os.path.isfile(local_filename):
                        files_to_process.append((local_filename, remote_dirname, remote_basename, header))
                        self.create_remote_path(name=remote_dirname)
                    elif os.path.isdir(local_filename):
                        dirs_to_process.append((local_filename, remote_dirname, remote_basename, header))
                    else:
                        raise NotImplementedError(f'Unimplemented management of "files" like {local_filename}')
                else:
                    raise ValueError(f"Local file {local_filename} declared at manifest {local_path} does not exist")
        
        while len(dirs_to_process) > 0:
            new_dirs_to_process = []
            for local_dir, upload_base_dir, upload_dir_basename, header in dirs_to_process:
                self.logger.debug(f'{local_dir} {upload_base_dir} {upload_dir_basename}')
                _ , remote_path, rel_remote_path = self.create_remote_path(upload_base_dir, upload_dir_basename)
                for entry in os.scandir(local_dir):
                    # Skipping problematic files,
                    if not entry.name.startswith('.'):
                        if entry.is_file():
                            files_to_process.append((entry.path, rel_remote_path, entry.name, header))
                        elif entry.is_dir():
                            new_dirs_to_process.append((entry.path, rel_remote_path, entry.name, header))
            
            # Now, rotation time
            dirs_to_process = new_dirs_to_process
        
        # No work, then return
        if len(files_to_process) == 0 and len(dirs_to_process) == 0:
            return
        
        retvals = self._chunked_file_batch_uploader(files_to_process)
        
        return retvals, retval_reldir, retval_relreldir
    
    def create_share_links(self, relpath, emails: List[str], expire_in: Optional[int] = None):
        retvals = []
        permissions = ExtendedNextcloudClient.OCS_PERMISSION_READ
        the_path = urllib.parse.quote(self.base_directory + '/' + relpath)
        
        if expire_in is not None:
            expire_at_d = datetime.date.today() + datetime.timedelta(days=expire_in)
            expire_at = expire_at_d.isoformat()
        else:
            expire_at = None
        
        if not isinstance(emails, (list, tuple)) or len(emails)==0:
            share_info = self.enc.share_file(the_path, ExtendedNextcloudClient.OCS_SHARE_TYPE_LINK, perms=permissions, expire_date=expire_at)
            retvals.append(share_info.get_link())
        else:
            for email in emails:
                share_info = self.enc.share_file(the_path, ExtendedNextcloudClient.OCS_SHARE_TYPE_EMAIL, share_dest=email, perms=permissions, expire_date=expire_at)
                share_link = share_info.get_link()
                if share_link is None:
                    share_token = share_info.get_token()
                    share_link = urllib.parse.urljoin(self.enc._webdav_url + '/', 'index.php/s/' + share_token)
                
                retvals.append(share_link)
        
        return retvals

class AuthHandler():

    def __init__(self, auth_url, client_id, client_secret, realm_name, insecure = True, cacert=None):

        """Instantiates handler for managing connections with Keycloak OpenID Protocol.
        :param auth_url: Base authentication url of KeyCloak server (e.g."https://my.keycloak:8443/auth"
        :param client_id: Client ID (according to OpenID Connect protocol).
        :param client_secret: Client secret (according to OpenID Connect protocol).
        :param realm_name: KeyCloak realm name.
        :cacert: SSL certificate file (Optional).
        :insecure: If True, SSL certificate is not verified (Optional).

        """

        # Getting a logger focused on specific classes
        self.logger = logging.getLogger(dict(inspect.getmembers(self))['__module__'] + '::' + self.__class__.__name__)
        self.logger.setLevel(logging.DEBUG)
        
        #handler = logging.StreamHandler(sys.stderr)
        #handler.setLevel(logging.DEBUG)
        #formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
        #handler.setFormatter(formatter)
        #self.logger.addHandler(handler)


        # Load Keycloak parameters
        self.auth_url      = auth_url.strip("/")
        self.client_id     = client_id 
        self.client_secret = client_secret
        self.realm_name    = realm_name 
        self.insecure      = insecure
        self.cacert        = cacert

    def __str__(self):
        return str(self.__class__) + '::' + str(self.__dict__)

    def authenticate_with_token_exchange(self, user_id):
        """Performs authentication via token exchange grant-type as implemented by Keycloak OpenID Protocol.
        :param user_id: Identifier of the user whose access_token is to be fetched.

        :return: Return HTTP response from the token Keycloak OIDC endpoint
        :rtype: JSON object
        """
        # Read and validate params

        self.user_id = user_id

        if not self.auth_url:
            raise ValueError('Base authentication url is not provided.')

        if not self.client_id:
            raise ValueError('Client ID is not provided.')

        if not self.realm_name:
            raise ValueError('Project(realm) name is not provided.')

        if not self.user_id:
            raise ValueError(
                'Requester mail or requester username must be provided.'
            )

        # Build POST request to OIDC provider

        #curl -X POST -d "client_id=fega-service-client"
        #             -d client_secret=xxxx"
        #              --data-urlencode "grant_type=urn:ietf:params:oauth:grant-type:token-exchange"
        #             -d "audience=fega-service-client"
        #             -d "requested_subject=test@bsc.es"
        #   https://inb.bsc.es/auth/realms/es-fega/protocol/openid-connect/token  

        access_token_endpoint = (
            "%s/realms/%s/protocol/openid-connect/token" %
            (self.auth_url, self.realm_name)
        )

        body = {
            'grant_type': 'urn:ietf:params:oauth:grant-type:token-exchange',
            'audience': self.client_id,
            'requested_subject': self.user_id,
            'client_id': self.client_id,
        }

        if self.client_secret:
            body['client_secret'] = self.client_secret,

        verify = None
        if urllib.parse.urlparse(access_token_endpoint).scheme == "https":
            verify = False if self.insecure else self.cacert if self.cacert else True
    
        # do POST request 
        resp = requests.post(
            access_token_endpoint,
            data=body,
            verify=verify
        )

        try:
            resp.raise_for_status()
        except Exception as e:
            raise Exception("Failed to get access token:\n %s" % str(e))


        format_headers = lambda d: '\n'.join(f'{k}: {v}' for k, v in d.items())
        debug_message = textwrap.dedent('''
            ---------------- request ----------------
            {req.method} {req.url}
            {reqhdrs}
    
            {req.body}
            ---------------- response ----------------
            {res.status_code} {res.reason} {res.url}
            {reshdrs}
    
            {res.text}
        ''').format(
            req=resp.request, 
            res=resp, 
            reqhdrs=format_headers(resp.request.headers), 
            reshdrs=format_headers(resp.headers), 
        )
        self.logger.debug("Access Token request to OIDC provider: %s",debug_message)
        #self.logger.debug("Access Token request to OIDC provider: %s", pprint.pformat(resp.json()))

        return resp.json()



    def get_userinfo(self, auth_token):
        """Get user attributes as exposed in the userinfo OIDC endpoint of Keycloak
        :param auth_token: Access token of the user to be queried. 
        :return: Return HTTP response from the userinfo Keycloak OIDC endpoint
        :rtype: JSON object
        """
        # Read and validate params

        self.auth_token = auth_token

        if not self.auth_url:
            raise ValueError('Base authentication url is not provided.')

        if not self.client_id:
            raise ValueError('Client ID is not provided.')

        if not self.realm_name:
            raise ValueError('Project(realm) name is not provided.')

        if not self.auth_token:
            raise ValueError(
                'Requester mail or requester username must be provided.'
            )

        # Build POST request to OIDC provider

        # curl  -X POST -H "Content-Type: application/x-www-form-urlencoded"
        #       -d "access_token=$KC_ACCESS_TOKEN"
        #       https://inb.bsc.es/auth/realms/es-fega/protocol/openid-connect/userinfo

        userinfo_endpoint = (
            "%s/realms/%s/protocol/openid-connect/userinfo" %
            (self.auth_url, self.realm_name)
        )

        body = {
            'access_token': self.auth_token
            #'scope': 'crypt4gh_public_key'
        }

        # do POST request 
        resp = requests.post(
            userinfo_endpoint,
            data=body,
            verify=None
        )

        try:
            resp.raise_for_status()
        except Exception as e:
            raise Exception("Failed to get access token:\n %s" % str(e))


        format_headers = lambda d: '\n'.join(f'{k}: {v}' for k, v in d.items())
        debug_message = textwrap.dedent('''
            ---------------- request ----------------
            {req.method} {req.url}
            {reqhdrs}
    
            {req.body}
            ---------------- response ----------------
            {res.status_code} {res.reason} {res.url}
            {reshdrs}
    
            {res.text}
        ''').format(
            req=resp.request, 
            res=resp, 
            reqhdrs=format_headers(resp.request.headers), 
            reshdrs=format_headers(resp.headers), 
        )
        self.logger.debug("User Info request to OIDC provider: %s",debug_message)

        return resp.json()


def main(argv) -> int:
    ap = argparse.ArgumentParser(
        description="FEGA NextCloud transport",
        formatter_class=argparse.ArgumentDefaultsHelpFormatter
    )
	
    ap.add_argument('-C', '--config', dest='config_filename', required=True, help='Configuration file with the nextcloud server connection details and the path to the vault\'s decryption key')
    ap.add_argument('vault_path', metavar='vault_dir_or_manifest_to_export', help='Manifest of encrypted files to export, or directory full of previously encrypted crypt4gh files')
    ap.add_argument('-pk', '--publish_key_or_filename_key', dest='publish_key_or_filename_key', required=False,  metavar='export_public_key', help='Public key to be used to re-encrypt the encrypted files in the vault, either as a file or inline.Either \'-pk\' or \'-u\' arguments are expected.')
    ap.add_argument('-e', '--email_addresses', dest='email_addresses', required=False, metavar='email_address', nargs='*', help='e-mail addresses of the receivers of the share link')
    ap.add_argument('-u', '--user', dest='user_id', required=False, metavar='user_id', help='e-mail or username of the fega account whose public key is to be used to re-encrypt the encrypted files in the vault. Either \'-pk\' or \'-u\' arguments are expected.')
    
    args = ap.parse_args()
    
    import pprint
    cp = configparser.ConfigParser()

    # Reading the public key to be reused on recoding
    if  args.publish_key_or_filename_key:
        if not os.path.exists(args.publish_key_or_filename_key):
            print(f"User\'s public crypt4GH key file {args.publish_key_or_filename_key} does not exist", file=sys.stderr)
            return 1
    else:
        if not args.user_id:
            print(f"No user\'s public crypt4GH key given. Please, set provide either '-u' or '-pk' parameters.", file=sys.stderr)
            return 1
        
    if os.path.exists(args.config_filename):
        # First, let's read the configuration file
        config_directory = os.path.dirname(args.config_filename)
        with open(args.config_filename, mode="r", encoding='utf-8') as cF:
            cp.read_file(cF, args.config_filename)
        # Now, let's extract the different sections
        for section_name in ('nextcloud', 'vault'):
            if not cp.has_section(section_name):
                print(f"Configuration file {args.config_filename} does not contain the {section_name} section", file=sys.stderr)
                return 2
        nSect = cp['nextcloud']
        vSect = cp['vault']
        # This section is optional, as all its parameters are so
        mSect = cp['manifest'] if cp.has_section('manifest') else {}
        # This section is optional, as yet dependent on user_id and publish_key_or_filename_key
        aSect = cp['fega-auth'] if cp.has_section('fega-auth') else {}
        if args.user_id and not aSect:
            print(f"Configuration file {args.config_filename} does not contain the 'fega-auth' section, required when parameter '-u' ({args.user_id}) is given.", file=sys.stderr)
            return 2
        
        vaultPrivKey = vSect['private-key']
        # Relative paths within the config file are interpreted relative to the configuration file
        if not os.path.isabs(vaultPrivKey):
            vaultPrivKey = os.path.normpath(os.path.join(config_directory, vaultPrivKey))
        
        vaultPrivKeyPF = vSect.get('private-key-passphrase')
        
        expire_in = None
        expire_in_str = nSect.get('shares-expire-in')
        if expire_in_str is not None:
            try:
                expire_in = int(expire_in_str)
            except ValueError:
                print(f"WARNING: shares-expire-in {expire_in_str} is not a valid integer", file=sys.stderr)
                return 3
        
        retention_tag_name = nSect.get('retention-tag', 'remove_in_14')
        
        if not args.publish_key_or_filename_key:

            print("No user\'s public cryp4GH key given. Using fega-outbox OIDC provider to fetch it")
            auth_handler = AuthHandler(
                aSect['auth_url'],
                aSect['client_id'],
                aSect['client_secret'],
                aSect['realm_name']
            )
            # Get access token of the fega user (data requester) via token exchange
            try:
                a_token = auth_handler.authenticate_with_token_exchange(args.user_id)['access_token']

            except KeyError as e: 
                print("Cannot get access token for the given fega user {}. \
Make sure it corresponds to a unique and searchable property (e.g. email or username).".format(args.user_id)
                )
                return 1

            #print("Auth token: %s" % a_token)

            # Get the 'crypt4gh_public_key' registered by the fega user in the OIDC provider
            try:
                crypt4gh_public_key = auth_handler.get_userinfo(a_token)['crypt4gh_public_key']

            except KeyError as e:
                print("Cannot find 'crypt4gh_public_key' for the given fega user {}. \
Make sure the user have it registered, as well as the corresponding client scope is enabled."
                .format(args.user_id)
                )
                return 1

            #crypt4gh_public_key="-----BEGIN CRYPT4GH PUBLIC KEY----- NUeHv2d3OTZ8X/1GHqaawQpxaw80AHHFyHERfEOWNXs= -----END CRYPT4GH PUBLIC KEY-----"
            print("Key successfully retrieved:\n%s" % crypt4gh_public_key)

            if len(crypt4gh_public_key.splitlines()) > 1:
                args.publish_key_or_filename_key = crypt4gh_public_key
            else:
                regex = re.compile('-----')
                extracted_key = regex.split(crypt4gh_public_key)[2:-2]
                #TODO: fails reading list
                args.publish_key_or_filename_key = ["CRYPT4GH", ''.join(extracted_key).strip()]

                print("Key successfully retrieved LIST: %s" % args.publish_key_or_filename_key)


        ce = ContentExporter(
            nSect['server'],
            nSect['user'],
            nSect['token'],
            nSect['base-directory'],
            args.publish_key_or_filename_key,
            vaultPrivKey,
            vaultPrivKeyPF,
            retention_tag_name=retention_tag_name
        )
        
        #uplodir, relpath, relrelpath = ce.create_remote_path('Con cariño')
        # uplodir , relpath, relrelpath = ce.create_remote_path()
        
        #with open(args.publish_key_filename, mode='rb') as uH:
        #    uplodir.upload_file_contents(uH, name=os.path.basename(args.publish_key_filename))
        #with open(args.publish_key_filename, mode='rb') as uH:
        #    uplodir.upload_file_contents(uH, name=os.path.basename(args.publish_key_filename))
        if os.path.isdir(args.vault_path):
            retvals , remote_path, remote_relpath = ce.content_uploader_from_dir(args.vault_path)
        elif os.path.isfile(args.vault_path):
            retvals , remote_path, remote_relpath = ce.content_uploader_from_manifest(
                args.vault_path,
                local_col=mSect.get('local-column-name', 'local'),
                remote_col=mSect.get('remote-column-name', 'remote'),
                header_col=mSect.get('header-column-name', 'header'),
            )
        else:
            print(f"{args.vault_path} is neither a file or a directory")
            return 3
        
        nak = False
        for i_retval, retval in enumerate(retvals):
            if not retval:
                nak = True
                print(f"There was some problem with operation {i_retval}")
        
        if nak:
            return 4
        
        shared_links = ce.create_share_links(remote_relpath, args.email_addresses, expire_in=expire_in)
        #import pprint
        #for shared_link in shared_links:
        #    pprint.pprint(shared_link.full_data)
        for shared_link in shared_links:
            print(shared_link)
        
        return 0
    else:
        print(f"Configuration file {args.config_filename} does not exist", file=sys.stderr)
        return 1

if __name__ == '__main__':
    sys.exit(main(sys.argv))
