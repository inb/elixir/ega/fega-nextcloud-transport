# Spain Federated EGA Nextcloud Transport script

This script is intended to be run from within the vault. It connects to a given NextCloud instance, with the credentials
declared at the input configuration file. A sample version of such configuration file is available at
[sample_config.ini.template](sample-config.ini.template).

Installation instructions of this script are available at [INSTALL.md](INSTALL.md).

## Usage

```bash
./fega-nc-transport.bash --help

usage: fega-nc-transport.py [-h] -C CONFIG_FILENAME [-pk export_public_key]
                            [-e [email_address [email_address ...]]] [-u user_id]
                            vault_dir_or_manifest_to_export

FEGA NextCloud transport

positional arguments:
  vault_dir_or_manifest_to_export
                        Manifest of encrypted files to export, or directory full of previously encrypted
                        crypt4gh files

optional arguments:
  -h, --help            show this help message and exit
  -C CONFIG_FILENAME, --config CONFIG_FILENAME
                        Configuration file with the nextcloud server connection details and the path to the
                        vault's decryption key (default: None)
  -pk export_public_key, --publish_key_or_filename_key export_public_key
                        Public key to be used to re-encrypt the encrypted files in the vault, either as a
                        file or inline.Either '-pk' or '-u' arguments are expected. (default: None)
  -e [email_address [email_address ...]], --email_addresses [email_address [email_address ...]]
                        e-mail addresses of the receivers of the share link (default: None)
  -u user_id, --user user_id
                        e-mail or username of the fega account whose public key is to be used to re-encrypt
                        the encrypted files in the vault. Either '-pk' or '-u' arguments are expected.
                        (default: None)
```

The input configuration file also allows setting up other behavioural
details, like:

* The expected names of the columns in the manifest files.
* The path to the master secret key needed to reencrypt encrypted files.
* The expiration time in days of the newly created share links.
* The name of the custom Nextcloud tag which is applied to all the
  created files and directories.

Typical command-line usages would be, 

```bash
python fega-nc-transport.py -C fega-nc-config.ini ADatasetDirectory -pk user-pk.key
python fega-nc-transport.py -C fega-nc-config.ini manifest_file.txt -u user_email

```

A manifest file should be a TSV or CSV file with at least two columns.
One of those columns should have the name 'local'

If you want to make some tests, you can generate key pairs using next command line:

```bash
crypt4gh-keygen --pk user-pk.key --sk user-sk.key --nocrypt -C 'Sample user key'

```
## Programmatic Downloads from created share links

The dataset can be accessed through WebDAV protocol. For instance, if the
Nextcloud server is reachable through https://my.nextcloud.server/ , the connection
server details are either https://my.nextcloud.server/public.php/webdav/
or davs://my.nextcloud.server/public.php/webdav/
or webdavs://my.nextcloud.server/public.php/webdav/

* If the share link is, for instance https://my.nextcloud.server/index.php/s/JnfXp6ydFA4NNZc ,
you should use as user the last part of the URL (in this example, `JnfXp6ydFA4NNZc`) ,
leaving empty the password field (unless the share link was set up with a password).

You can use both command line tools and any WebDAV client, for instance:

* [WinSCP](https://winscp.net/) (for Windows users).

* [Cyberduck](https://cyberduck.io/) (for Windows or macOS users).

* Both [Nautilus](http://help.collab.net/index.jsp?topic=/teamforge171/action/connecttowebdavwithnautilus.html)
  and Dolphin support mounting and browsing WebDAV resources through extensions.

* [cadaver](http://www.webdav.org/cadaver/) (command-line Linux/*NIX client).
