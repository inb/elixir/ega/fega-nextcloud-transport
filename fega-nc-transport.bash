#!/bin/bash

# Getting the installation directory
fegaTransDir="$(dirname "$0")"
case "${fegaTransDir}" in
	/*)
		# Path is absolute
		true
		;;
	*)
		# Path is relative
		fegaTransDir="$(readlink -f "${fegaTransDir}")"
		;;
esac


envDir="$(python3 -c 'import sys; print(""  if sys.prefix==sys.base_prefix  else  sys.prefix)')"
if [ -z "${envDir}" ] ; then
	envDir="${fegaTransDir}/.f"

#	echo "Creating fega-nc-transport python virtual environment at ${envDir}"

	# Checking whether the environment exists
	if [ ! -f "${envActivate}" ] ; then
		python3 -m venv "${envDir}"
	fi

	# Activating the python environment
	envActivate="${envDir}/bin/activate"
	source "${envActivate}"
#else
#	echo "Using currently active environment ${envDir} to install the dependencies"
fi

# Checking whether the modules were already installed
if ! python -c 'import nextcloud; import crypt4gh' 2> /dev/null ; then
	echo "Installing fega-nc-transport python dependencies"
	pip install --upgrade pip wheel
	pip install -r "${fegaTransDir}"/requirements.txt -c "${fegaTransDir}"/constraints.txt
fi

exec python "$(basename "$0" .bash)".py "$@"
